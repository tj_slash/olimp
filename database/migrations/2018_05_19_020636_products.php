<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->index();
            $table->string('title');
            $table->string('slug')->unique();
            $table->longText('text');
            $table->longText('characters')->nullable()->default(null);
            $table->json('images');
            $table->json('videos')->nullable()->default(null);
            $table->decimal('price')->nullable()->default(null)->index();
            $table->boolean('is_recommended')->default(false);
            $table->string('meta_title')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->string('meta_description')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
