<?php
use Illuminate\Database\Seeder;
use App\Models\Category;
use App\Models\Parametr;

class ParametrSeeder extends Seeder
{
    public function run()
    {
        Parametr::truncate();

        $categories = Category::all();

        foreach ($categories as $category) {
        	Parametr::create(['category_id' => $category->id, 'title' => 'Размер 1 ед']);
        	Parametr::create(['category_id' => $category->id, 'title' => 'Масса 1 ед']);
        	Parametr::create(['category_id' => $category->id, 'title' => 'Цена 1 ед']);
        	Parametr::create(['category_id' => $category->id, 'title' => 'Масса 1 м']);
        	Parametr::create(['category_id' => $category->id, 'title' => 'Цена 1 м']);
        	Parametr::create(['category_id' => $category->id, 'title' => 'Цена за тонну']);
        }
    }
}
