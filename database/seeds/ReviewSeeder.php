<?php

use Illuminate\Database\Seeder;
use App\Models\Review;

class ReviewSeeder extends Seeder
{
    public function run()
    {
        Review::truncate();
        factory(Review::class, 100)->create();
    }
}
