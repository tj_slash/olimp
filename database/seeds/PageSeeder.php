<?php
use Illuminate\Database\Seeder;
use App\Models\Page;

class PageSeeder extends Seeder
{
    public function run()
    {
        Page::truncate();
        factory(Page::class)->create(['title' => 'Документы']);
        factory(Page::class)->create(['title' => 'О компании']);
        factory(Page::class)->create(['title' => 'Широкий ассортимент продукции']);
        factory(Page::class)->create(['title' => 'Предоставляем полный комплекс услуг']);
        factory(Page::class)->create(['title' => 'Договорная ценовая политика']);
    }
}
