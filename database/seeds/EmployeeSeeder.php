<?php
use Illuminate\Database\Seeder;
use App\Models\Employee;

class EmployeeSeeder extends Seeder
{
    public function run()
    {
    	File::delete(File::allFiles('public/uploads/employees/'));
        Employee::truncate();
        factory(Employee::class, 6)->create();
    }
}
