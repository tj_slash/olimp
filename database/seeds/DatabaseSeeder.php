<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(PageSeeder::class);
        $this->call(ArticleSeeder::class);
        $this->call(EmployeeSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(ParametrSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(ValueSeeder::class);
        $this->call(ReviewSeeder::class);
        $this->call(SlideSeeder::class);
    }
}
