<?php
use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    public function run()
    {
        User::truncate();
        factory(User::class)->create([
        	'name' => 'Администратор',
        	'email' => 'admin@site.com',
            'password' => bcrypt('password')
    	]);
    }
}
