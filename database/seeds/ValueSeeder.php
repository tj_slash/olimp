<?php
use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\Parametr;
use App\Models\Value;

class ValueSeeder extends Seeder
{
    public function run()
    {
        Value::truncate();

        $products = Product::all();

        foreach ($products as $product) {
	        foreach ($product->category->parametrs as $parametr) {
	        	factory(Value::class)->create([
	        		'product_id' => $product->id,
	        		'parametr_id' => $parametr->id
        		]);
	        }
        }
    }
}
