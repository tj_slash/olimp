<?php
use Illuminate\Database\Seeder;
use App\Models\Article;

class ArticleSeeder extends Seeder
{
    public function run()
    {
    	File::delete(File::allFiles('public/uploads/articles/'));
        Article::truncate();
        factory(Article::class, 30)->create();
    }
}
