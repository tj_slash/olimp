<?php
use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductSeeder extends Seeder
{
    public function run()
    {
    	File::delete(File::allFiles('public/uploads/products/'));
        Product::truncate();
        factory(Product::class, 100)->create();
    }
}