<?php
use Illuminate\Database\Seeder;
use App\Models\Slide;

class SlideSeeder extends Seeder
{
    public function run()
    {
    	File::delete(File::allFiles('public/uploads/slides/'));
        Slide::truncate();
        factory(Slide::class, 5)->create();
    }
}
