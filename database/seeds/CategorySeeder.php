<?php
use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    public function run()
    {
        File::delete(File::allFiles('public/uploads/categories/'));
        File::delete(File::allFiles('public/uploads/prices/'));
        Category::truncate();
        factory(Category::class)->create(['title' => 'Сортовый прокат']);
        factory(Category::class)->create(['title' => 'Фасонный прокат']);
        factory(Category::class)->create(['title' => 'Листовой прокат']);
        factory(Category::class)->create(['title' => 'Трубный прокат']);
        factory(Category::class)->create(['title' => 'Метизная продукция']);
        factory(Category::class)->create(['title' => 'Профнастил']);
    }
}
