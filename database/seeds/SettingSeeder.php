<?php
use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingSeeder extends Seeder
{
    public function run()
    {
        Setting::truncate();
        Setting::create([
        	'title' => 'Номер телефона',
        	'slug' => 'phone',
        	'value' => '+7 (914) 567-40-14',
    	]);
        Setting::create([
        	'title' => 'E-mail',
        	'slug' => 'email',
        	'value' => 'olimp.metall@mail.ru',
    	]);
        Setting::create([
        	'title' => 'Адрес офиса',
        	'slug' => 'address',
        	'value' => 'г. Благовещенск, Переулок Угловой, 35а',
    	]);
        Setting::create([
        	'title' => 'Кол-во статей на главной',
        	'slug' => 'count_articles_main',
        	'value' => '4',
    	]);
        Setting::create([
        	'title' => 'E-mail для оповещений',
        	'slug' => 'noty_email',
        	'value' => 'olimp.metall@mail.ru',
    	]);
    }
}
