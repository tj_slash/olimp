<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Parametr::class, function (Faker $faker) {
    return [
        'category_id' => $faker->randomDigitNotNull,
        'title' => $faker->sentence,
    ];
});

