<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Slide::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'image' => '/uploads/slides/' . $faker->image('public/uploads/articles/', 1100, 400, 'cats', false),
        'url' => $faker->url
    ];
});
