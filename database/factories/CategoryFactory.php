<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Category::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'image' => '/uploads/categories/' . $faker->image('public/uploads/categories/', 500, 300, 'cats', false),
        'image' => '/uploads/categories/' . $faker->image('public/uploads/categories/', 1100, 400, 'cats', false),
        'text' => $faker->text,
        'price_list' => '/uploads/prices/' . $faker->image('public/uploads/prices/', 10, 10, 'cats', false),
        'meta_title' => $faker->sentence,
        'meta_keywords' => $faker->sentence,
        'meta_description' => $faker->text
    ];
});