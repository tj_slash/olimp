<?php
$factory->define(App\Models\Product::class, function (Faker\Generator $faker) {
	$faker->addProvider(new \Faker\Provider\Youtube($faker));
	$categories = \App\Models\Category::all()->pluck('id')->toArray();
    return [
        'category_id' => $faker->randomElement($categories),
        'title' => $faker->word,
        'text' => $faker->paragraph,
        'characters' => $faker->paragraph,
        'images' => [
        	'/uploads/products/' . $faker->image('public/uploads/products/', 500, 300, 'cats', false),
        	'/uploads/products/' . $faker->image('public/uploads/products/', 500, 300, 'cats', false),
        	'/uploads/products/' . $faker->image('public/uploads/products/', 500, 300, 'cats', false),
        ],
        'videos' => [
        	$faker->youtubeUri(),
        	$faker->youtubeUri(),
        	$faker->youtubeUri(),
        ],
        'price' => $faker->randomFloat(2, 1, 300000),
        'is_recommended' => $faker->boolean(15),
        'meta_title' => $faker->sentence,
        'meta_keywords' => $faker->sentence,
        'meta_description' => $faker->text
    ];
});
