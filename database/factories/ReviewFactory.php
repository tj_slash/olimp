<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Review::class, function (Faker $faker) {
    $products = \App\Models\Product::all()->pluck('id')->toArray();
    return [
        'product_id' => $faker->randomElement($products),
		'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
		'phone' => $faker->e164PhoneNumber,
		'text' => $faker->text,
    ];
});
