<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Value::class, function (Faker $faker) {
    $parametrs = \App\Models\Parametr::all()->pluck('id')->toArray();
    $products = \App\Models\Product::all()->pluck('id')->toArray();
    return [
        'product_id' => $faker->randomElement($products),
		'parametr_id' => $faker->randomElement($parametrs),
		'value' => $faker->randomDigitNotNull
    ];
});
