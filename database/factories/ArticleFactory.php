<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Article::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'image' => '/uploads/articles/' . $faker->image('public/uploads/articles/', 500, 300, 'cats', false),
        'anonce' => $faker->text,
        'text' => $faker->paragraph,
        'is_new' => $faker->boolean(15),
        'meta_title' => $faker->sentence,
        'meta_keywords' => $faker->sentence,
        'meta_description' => $faker->text
    ];
});
