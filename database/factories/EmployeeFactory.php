<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Employee::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'position' => $faker->sentence,
        'email' => $faker->email,
        'phone' => $faker->e164PhoneNumber,
        'image' => '/uploads/employees/' . $faker->image('public/uploads/employees/', 300, 300, 'cats', false),
    ];
});