<?php
// Home
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', route('home'));
});

// Contacts
Breadcrumbs::register('contacts', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Контакты', route('contacts'));
});

// About
Breadcrumbs::register('about', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('О компании', route('about'));
});

// Page
Breadcrumbs::register('page', function ($breadcrumbs, $page) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push($page->title, route('page', $page->slug));
});

// Articles
Breadcrumbs::register('articles', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Статьи', route('articles'));
});
Breadcrumbs::register('article', function ($breadcrumbs, $article) {
    $breadcrumbs->parent('articles');
    $breadcrumbs->push($article->title, route('article.view', $article->slug));
});

// Catalog
Breadcrumbs::register('catalog', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Каталог', route('catalog'));
});
Breadcrumbs::register('category', function ($breadcrumbs, $category) {
    $breadcrumbs->parent('catalog');
    $breadcrumbs->push($category->title, route('category', $category->slug));
});
Breadcrumbs::register('search', function ($breadcrumbs, $q) {
    $breadcrumbs->parent('catalog');
    $breadcrumbs->push("Поиск товаров: $q", route('search'));
});
Breadcrumbs::register('product', function ($breadcrumbs, $product) {
    $breadcrumbs->parent('category', $product->category);
    $breadcrumbs->push($product->title, route('product', [$product->category->slug, $product->slug]));
});
