<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () {
	CRUD::resource('user', 'UserCrudController');
	CRUD::resource('page', 'PageCrudController');
	CRUD::resource('article', 'ArticleCrudController');
	CRUD::resource('category', 'CategoryCrudController');
	CRUD::resource('product', 'ProductCrudController');
	CRUD::resource('setting', 'SettingCrudController');
	CRUD::resource('employee', 'EmployeeCrudController');
	CRUD::resource('feedback', 'FeedbackCrudController');
	CRUD::resource('subscribe', 'SubscribeCrudController');
	CRUD::resource('parametr', 'ParametrCrudController');
	CRUD::resource('value', 'ValueCrudController');
	CRUD::resource('get', 'GetCrudController');
	CRUD::resource('bid', 'BidCrudController');
	CRUD::resource('review', 'ReviewCrudController');
	CRUD::resource('call', 'CallCrudController');
	CRUD::resource('slide', 'SlideCrudController');
});