<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

/**
 * Image resizer
 */
Route::get('/image/{h}/{w}/{img?}',function($height = 200, $width = 200, $img) {
	$image = Image::cache(function($image) use ($img, $height, $width) {
	    $image->make(public_path($img))->fit($height, $width);
	});
	return response($image, 200)->header('Content-Type', 'image/jpg');
})->name('image')->where('img', '(.*)');

/**
 * Main page
 */
Route::get('/', 'HomeController@index')->name('home');

/**
 * Articles
 */
Route::group(['prefix' => 'articles'], function () {
	Route::get('', 'ArticleController@index')->name('articles');
	Route::get('{slug}', 'ArticleController@view')->name('article.view');
});

/**
 * Contacts
 */
Route::get('contacts', 'HomeController@contacts')->name('contacts');

/**
 * Pages
 */
Route::get('page/{slug}', 'PageController@index')->name('page');

/**
 * About
 */
Route::get('about', 'HomeController@about')->name('about');

/**
 * Subscribe
 */
Route::post('subscribe', 'HomeController@subscribe')->name('subscribe');
Route::post('price-list', 'HomeController@priceList')->name('price-list');
Route::post('bid', 'HomeController@bid')->name('bid');
Route::post('call', 'HomeController@call')->name('call');

/**
 * Catalog
 */
Route::group(['prefix' => 'catalog'], function () {
	Route::get('', 'CatalogController@index')->name('catalog');
	Route::get('search', 'CatalogController@search')->name('search');
	Route::get('{category}', 'CatalogController@category')->name('category');
	Route::get('{category}/{product}', 'CatalogController@product')->name('product');
});