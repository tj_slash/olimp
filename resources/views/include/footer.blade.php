<footer class="footer padding-1">
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell small-12 medium-4 margin-bottom-1">
				<a href="{{ route('home') }}" title="{{ config('app.name', 'Laravel') }}">
					<img src="{{ asset('/uploads/images/logotype_footer.svg') }}" title="{{ config('app.name', 'Laravel') }}" alt="{{ config('app.name', 'Laravel') }}">
				</a>
			</div>
			<div class="cell small-6 medium-2 margin-bottom-1">
				<h6 class="footer__title text-uppercase">
					Продукция
				</h6>
				<ul class="menu vertical footer__menu">
					@foreach($categories as $category)
						<li>
							<a href="{{ route('category', [$category->slug]) }}" title="{{ $category->title }}" class="footer__menu__link">
								{{ $category->title }}
							</a>
						</li>
					@endforeach
				</ul>
			</div>
			<div class="cell small-6 medium-2 margin-bottom-1">
				<h6 class="footer__title text-uppercase">
					Навигация
				</h6>
				<ul class="menu vertical footer__menu">
					<li>
						<a href="{{ route('home') }}" title="{{ config('app.name', 'Laravel') }}" class="footer__menu__link">
							Главная
						</a>
					</li>
					<li>
						<a href="{{ route('catalog') }}" title="Каталог" class="footer__menu__link">
							Каталог
						</a>
					</li>
					<li>
						<a href="{{ route('articles') }}" title="Статьи" class="footer__menu__link">
							Статьи
						</a>
					</li>
					<li>
						<a href="{{ route('page', 'dokumenty')}}" title="Документы" class="footer__menu__link">
							Документы
						</a>
					</li>
					<li>
						<a href="{{ route('about') }}" title="О компании" class="footer__menu__link">
							О компании
						</a>
					</li>
					<li>
						<a href="{{ route('contacts') }}" title="Сотрудники" class="footer__menu__link">
							Сотрудники
						</a>
					</li>
				</ul>
			</div>
			<div class="cell small-12 medium-4 margin-bottom-1">
				<h6 class="footer__title text-uppercase">
					Подписаться на рассылку
				</h6>
				<subscribe></subscribe>
			</div>
		</div>
	</div>
</footer>