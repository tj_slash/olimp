<header-block class="header" inline-template>
	<div>
		<div class="grid-container height-100">
			<div class="show-for-small-only grid-x grid-margin-x height-100 flex-container align-middle">
				<div class="cell small-6">
					<a href="{{ route('home') }}" title="{{ config('app.name', 'Laravel') }}">
						<img src="{{ asset('/uploads/images/logotype_footer.svg') }}" title="{{ config('app.name', 'Laravel') }}" alt="{{ config('app.name', 'Laravel') }}">
					</a>
				</div>
				<div class="cell small-6 text-right">
					<a class="header__menu_button" @click="open = true">
						<i class="mdi mdi-menu"></i> меню
					</a>
				</div>
			</div>
			<div class="hide-for-small-only grid-x grid-margin-x height-100 flex-container align-middle">
				<div class="cell small-12 medium-2 text-center medium-text-left">
					<a href="{{ route('home') }}" title="{{ config('app.name', 'Laravel') }}">
						<img src="{{ asset('/uploads/images/logotype_text.svg') }}" title="{{ config('app.name', 'Laravel') }}" alt="{{ config('app.name', 'Laravel') }}">
					</a>
				</div>
				<div class="cell small-12 medium-3">
					<form action="{{ route('search') }}" method="GET">
						@csrf
						<div class="header__search">
				            <input type="search" name="q" class="margin-0 header__search__input"  value="{{ request()->q }}" placeholder="Поиск по сайту" required/>
				            <button type="submit" class="header__search__button button warning">
								<i class="mdi mdi-magnify"></i>
							</button>
				        </div>
					</form>
				</div>
				<div class="cell small-12 medium-2 medium-text-center hide-for-small-only">
					<a href="{{ route('home') }}" title="{{ config('app.name', 'Laravel') }}">
						<img src="{{ asset('/uploads/images/logotype.svg') }}" title="{{ config('app.name', 'Laravel') }}" alt="{{ config('app.name', 'Laravel') }}">
					</a>
				</div>
				<div class="cell small-12 medium-2">
					<a href="tel:{{ config('settings.phone') }}" title="{{ config('settings.phone') }}" target="_blank" class="header__phone">
						<strong>{{ config('settings.phone') }}</strong>
					</a>
				</div>
				<div class="cell small-12 medium-2">
					<call></call>
				</div>
				<div class="cell small-12 medium-1 text-right">
					<a class="header__menu_button" @click="open = true">
						<strong><i class="mdi mdi-menu"></i> меню</strong>
					</a>
				</div>
			</div>
		</div>
		<div v-show="open" style="display:none" class="header__menu">
			<div class="grid-container height-100">
				<div class="grid-x grid-margin-x">
					<div class="cell small-6 flex-container padding-top-3">
						<ul class="menu vertical header__menu__ul">
							<li>
								<a href="{{ route('home') }}" title="{{ config('app.name', 'Laravel') }}" class="header__menu__ul__a text-uppercase">
									Главная
								</a>
							</li>
							<li>
								<a href="{{ route('about') }}" title="О компании" class="header__menu__ul__a text-uppercase">
									О компании
								</a>
							</li>
							<li>
								<a href="{{ route('catalog') }}" title="Каталог" class="header__menu__ul__a text-uppercase">
									Каталог
								</a>
							</li>
							<li>
								<a href="{{ route('articles') }}" title="Статьи" class="header__menu__ul__a text-uppercase">
									Статьи
								</a>
							</li>
							<li>
								<a href="{{ route('page', 'dokumenty')}}" title="Документы" class="header__menu__ul__a text-uppercase">
									Документы
								</a>
							</li>
							<li>
								<a href="{{ route('contacts') }}" title="Контакты" class="header__menu__ul__a text-uppercase">
									Контакты
								</a>
							</li>
						</ul>
					</div>
					<div class="cell small-6 text-right padding-top-3">
						<a class="header__menu__button" @click="open = false">
							<i class="mdi mdi-close"></i> меню
						</a>
					</div>
				</div>
				<div class="margin-top-2 padding-top-2">
					<div class="grid-x grid-margin-x header__menu__footer">
						<div class="cell small-12 medium-4">
							<h6 class="header__menu__footer__title text-uppercase display-inline-block">
								Связаться с нами
							</h6>
							<p>
								E-mail: <a href="mailto:{{ config('settings.email') }}" target="_blank">{{ config('settings.email') }}</a>
							</p>
							<p>
								Телефон: <a href="tel:{{ config('settings.phone') }}" target="_blank">{{ config('settings.phone') }}</a>
							</p>
						</div>
						<div class="cell small-12 medium-4">
							<h6 class="header__menu__footer__title text-uppercase display-inline-block">
								Адрес офиса и склада
							</h6>
							<p>
								{{ config('settings.address') }}
							</p>
						</div>
						<div class="cell small-12 medium-4">
							<a class="header__menu__footer__title text-uppercase white display-inline-block" href="{{ route('contacts') }}">
								<i class="mdi mdi-map-marker"></i> Мы на карте
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header-block>