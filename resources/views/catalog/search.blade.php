@extends('layouts.app')

@section('title', 'Поиск товаров')
@section('description', 'Поиск товаров')
@section('keywords', 'Поиск товаров')

@section('content')
	<div class="catalog">
		<div class="header_wrapper">
			<div class="grid-container header_wrapper_m">
				<h1 class="header_title">
					Поиск товаров
				</h1>
			</div>
		</div>
		<div class="grid-container">
			{{ Breadcrumbs::render('search', request()->q) }}
		</div>
		<div class="grid-container">
			<div class="grid-x align-stretch catalog__products">
				<div class="cell small-12 medium-3 margin-bottom-1 align-stretch align-middle">
					<div class="catalog__categories height-100 padding-2">
						<h3 class="catalog__categories__header margin-bottom-1 text-uppercase">
							Каталог
						</h3>
						<ul class="menu vertical catalog__categories__menu">
							@foreach($categories as $category)
								<li>
									<a href="{{ route('category', [$category->slug]) }}" title="{{ $category->title }}" class="catalog__categories__menu__link">
										{{ $category->title }}
									</a>
								</li>
							@endforeach
						</ul>
					</div>
				</div>
				@foreach($products as $product)
					<div class="cell small-12 medium-3 margin-bottom-1 align-stretch">
						@include('catalog/item')
					</div>
				@endforeach
			</div>
			<div class="grid-x grid-margin-x">
				<div class="cell small-12 margin-bottom-1 text-center">
					{!! $products->appends(\Request::except('page'))->render() !!}
				</div>
			</div>
		</div>
	</div>
@endsection