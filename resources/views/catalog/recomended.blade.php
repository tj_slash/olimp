<div class="grid-container">
	<h4 class="header_title small margin-bottom-1">
		Рекомендуемые товары
	</h4>
	<div class="grid-x align-stretch catalog__products margin-bottom-1">
		@foreach($products as $product)
			<div class="cell small-12 medium-3 margin-bottom-1 align-stretch">
				@include('catalog/item')
			</div>
		@endforeach
	</div>
</div>