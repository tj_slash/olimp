@extends('layouts.app')

@section('title', $category->meta_title)
@section('description', $category->meta_description)
@section('keywords', $category->meta_keywords)

@section('content')
	<div class="catalog">
		<div class="header_wrapper">
			<div class="grid-container header_wrapper_m">
				<h1 class="header_title">
					{{ $category->title }}
				</h1>
			</div>
		</div>
		<div class="grid-container">
			{{ Breadcrumbs::render('category', $category) }}
		</div>
		<div class="catalog__header margin-bottom-1" style="background-image:url({{ $category->background ? '/' . $category->background : '/uploads/images/background.jpg' }})">
			<div class="grid-container padding-vertical-1">
				<div class="grid-x grid-margin-x">
					<div class="cell small-12 medium-6">
						<div class="catalog__header__text padding-1">
							{{ $category->text }}
						</div>
					</div>
					<div class="cell small-12 medium-6">
						<div class="text-center">
							<img src="{{ route('image', [360, 360, $category->image]) }}" title="{{ $category->title }}" alt="{{ $category->title }}" class="catalog__header__image">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="grid-container">
			<div class="grid-x grid-margin-x flex-container align-middle catalog__filter">
				<div class="cell small-12 medium-5">
					<price-list :price-list="'{{ $category->price_list }}'"></price-list>
				</div>
			</div>
		</div>
		<div class="grid-container">
			<div class="grid-x align-stretch catalog__products">
				@foreach($products as $product)
					<div class="cell small-12 medium-3 margin-bottom-1 align-stretch">
						@include('catalog/item')
					</div>
				@endforeach
			</div>
			<div class="grid-x grid-margin-x">
				<div class="cell small-12 margin-bottom-1 text-center">
					{{ $products->links() }}
				</div>
			</div>
		</div>
	</div>
@endsection