<div class="catalog__products__product height-100">
	<a href="{{ route('product', [$product->category->slug, $product->slug]) }}" title="{{ $product->title }}">
		<img src="{{ route('image', [240, 120, $product->image]) }}" title="{{ $product->title }}" alt="{{ $product->title }}" class="width-100">
	</a>
	<div class="padding-1">
		<a href="{{ route('product', [$product->category->slug, $product->slug]) }}" title="{{ $product->title }}" class="display-block margin-bottom-1 catalog__products__product__title text-uppercase">
			{{ $product->title }}
		</a>
		<div class="margin-bottom-1">
			@foreach($product->values as $value)
				<div class="catalog__products__product__character">
					{{ $value->parametr->title }}: <strong>{{ $value->value }}</strong>
				</div>
			@endforeach
		</div>
		<div class="medium-text-right catalog__products__product__more">
			<a href="{{ route('product', [$product->category->slug, $product->slug]) }}" title="{{ $product->title }}" class="catalog__products__product__buy">
				подробнее
			</a>
		</div>
	</div>
</div>