@extends('layouts.app')

@section('title', $product->meta_title)
@section('description', $product->meta_description)
@section('keywords', $product->meta_keywords)

@section('content')
	<div class="catalog">
		<div class="header_wrapper">
			<div class="grid-container header_wrapper_m">
				<h1 class="header_title">
					{{ $product->title }}
				</h1>
			</div>
		</div>
		<div class="grid-container">
			{{ Breadcrumbs::render('product', $product) }}
		</div>
		<div class="grid-container">
			<div class="grid-x grid-margin-x margin-bottom-3">
				<div class="cell small-12 medium-3 margin-bottom-1 align-middle hide-for-small-only">
					<div class="catalog__categories catalog__product padding-2 margin-bottom-1" style="border:none">
						<h3 class="catalog__categories__header margin-bottom-1 text-uppercase">
							Каталог
						</h3>
						<ul class="menu vertical catalog__categories__menu">
							@foreach($categories as $category)
								<li>
									<a href="{{ route('category', [$category->slug]) }}" title="{{ $category->title }}" class="catalog__categories__menu__link">
										{{ $category->title }}
									</a>
								</li>
							@endforeach
						</ul>
					</div>
					<price-list class="width-100" :price-list="'{{ $category->price_list }}'"></price-list>
				</div>
				<div class="cell small-12 medium-9 margin-bottom-1 catalog__product">
					<div class="grid-x">
						<div class="cell small-12 medium-6 margin-bottom-1">
							<div class="padding-2">
								<h2 class="display-block margin-bottom-1 catalog__product__title text-uppercase">
									{{ $product->title }}
								</h2>
								<div class="margin-bottom-1">
									@foreach($product->values as $value)
										<div class="catalog__product__character margin-bottom-1">
											{{ $value->parametr->title }}: <strong>{{ $value->value }}</strong>
										</div>
									@endforeach
								</div>
								<div class="margin-bottom-1">
									<bid :product-id="{{ $product->id }}"></bid>
								</div>
							</div>
						</div>
						<div class="cell small-12 medium-6 margin-bottom-1">
							<img src="{{ route('image', [340, 340, $product->image]) }}" title="{{ $product->title }}" alt="{{ $product->title }}" class="width-100">
						</div>
						<div class="cell small-12">
							<accordion class="accordion">
							    <h3 slot="heading" slot-scope="open" class="accordion__link text-uppercase padding-horizontal-2" v-bind:class="{accordion__active: open.open}">
							    	Подробное описание <i class="mdi" v-bind:class="{'mdi-chevron-down': open.open, 'mdi-chevron-up': !open.open}"></i>
							    </h3>
							    <div slot="content" class="accordion__content padding-horizontal-2 margin-bottom-1">
							    	{!! $product->text !!}
							    </div>
						    </accordion>
						    @if (!empty($product->characters))
								<accordion class="accordion">
								    <h3 slot="heading" slot-scope="open" class="accordion__link text-uppercase padding-horizontal-2" v-bind:class="{accordion__active: open.open}">
								    	Технические характеристики <i class="mdi" v-bind:class="{'mdi-chevron-down': open.open, 'mdi-chevron-up': !open.open}"></i>
								    </h3>
								    <div slot="content" class="accordion__content padding-horizontal-2 margin-bottom-1">
								    	{!! $product->characters !!}
								    </div>
							    </accordion>
						    @endif
						    @if (!empty($product->images) || !empty($product->videos))
								<accordion class="accordion">
								    <h3 slot="heading" slot-scope="open" class="accordion__link text-uppercase padding-horizontal-2" v-bind:class="{accordion__active: open.open}">
								    	Видео / фото <i class="mdi" v-bind:class="{'mdi-chevron-down': open.open, 'mdi-chevron-up': !open.open}"></i>
								    </h3>
								    <div slot="content" class="accordion__content padding-horizontal-2 margin-bottom-1">
									    @if (!empty($product->images))
									    	<gallery :images="{{ collect($product->images)->map(function($image) { return '/' . $image; })->toJson() }}"></gallery>
						    			@endif
						    			@if (!empty($product->videos) && !empty($product->videos['id']))
						    				<iframe width="560" height="315" src="https://www.youtube.com/embed/{{ $product->videos['id'] }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
						    			@endif
								    </div>
							    </accordion>
						    @endif
						</div>
					</div>
				</div>
			</div>
		</div>
		{!! \App\Http\Controllers\CatalogController::recomended($product) !!}
	</div>
@endsection