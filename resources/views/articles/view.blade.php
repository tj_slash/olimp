@extends('layouts.app')

@section('title', $article->meta_title)
@section('description', $article->meta_description)
@section('keywords', $article->meta_keywords)

@section('content')
	<div class="header_wrapper">
		<div class="grid-container header_wrapper_m">
			<h1 class="header_title">
				Статьи
			</h1>
		</div>
	</div>
	<div class="grid-container margin-bottom-1">
		{{ Breadcrumbs::render('article', $article) }}
		<div class="grid-x article">
			<div class="cell small-12 article__item">
				<img src="{{ route('image', [460, 350, $article->image]) }}" title="{{ $article->title }}" alt="{{ $article->title }}" class="float-left" style="margin:0 1rem 1rem 0">
				<div class="padding-1">
					<span class="label warning rounded margin-bottom-1">
						<strong>Свежая статья</strong>
					</span>
					<h2 class="margin-bottom-1 article__item__title text-uppercase">
						{{ $article->title }}
					</h2>
					<div class="margin-bottom-1">
						{!! $article->text !!}
					</div>
					<div class="clearfix flex-container align-middle">
						<div class="float-left width-50">
							<a href="{{ route('articles') }}" title="Статьи" class="article__item__back">
								<strong>вернуться к списку статей</strong>
							</a>
						</div>
						<div class="float-right text-right article__item__date width-50">
							{{ $article->created_at }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection