@extends('layouts.app')

@section('title', 'Статьи')
@section('description', 'Статьи')
@section('keywords', 'Статьи')

@section('content')
	<div class="header_wrapper">
		<div class="grid-container header_wrapper_m">
			<h1 class="header_title">
				Статьи
			</h1>
		</div>
	</div>
	<div class="grid-container articles">
		{{ Breadcrumbs::render('articles') }}
		<div class="grid-x align-stretch articles__list">
			@if (!empty($article))
				<div class="cell small-12 margin-bottom-1">
					<div class="grid-x articles__list__item">
						<div class="cell small-12 medium-6">
							<a href="{{ route('article.view', [$article->slug]) }}" title="{{ $article->title }}">
								<img src="{{ route('image', [460, 350, $article->image]) }}" title="{{ $article->title }}" alt="{{ $article->title }}" class="width-100">
							</a>
						</div>
						<div class="cell small-12 medium-6">
							<div class="padding-1">
								<span class="label warning rounded margin-bottom-1">
									<strong>Свежая статья</strong>
								</span>
								<a href="{{ route('article.view', [$article->slug]) }}" title="{{ $article->title }}" class="display-block margin-bottom-1 articles__list__item__title text-uppercase">
									{{ $article->title }}
								</a>
								<p class="articles__list__item__anonce margin-bottom-1">
									{{ $article->anonce }}
								</p>
								<div class="clearfix flex-container align-middle">
									<div class="float-left width-50">
										<a href="{{ route('article.view', [$article->slug]) }}" title="{{ $article->title }}" class="articles__list__item__more">
											<strong>читать</strong>
										</a>
									</div>
									<div class="float-right text-right articles__list__item__date width-50">
										{{ $article->created_at }}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			@endif
			@foreach($articles as $article)
				<div class="cell small-12 medium-3 margin-bottom-1 align-stretch">
					<div class="articles__list__item height-100">
						<a href="{{ route('article.view', [$article->slug]) }}" title="{{ $article->title }}">
							<img src="{{ route('image', [240, 120, $article->image]) }}" title="{{ $article->title }}" alt="{{ $article->title }}" class="width-100">
						</a>
						<div class="padding-1">
							<a href="{{ route('article.view', [$article->slug]) }}" title="{{ $article->title }}" class="display-block margin-bottom-1 articles__list__item__title text-uppercase">
								{{ $article->title }}
							</a>
							<p class="articles__list__item__anonce margin-bottom-1">
								{{ $article->anonce }}
							</p>
							<div class="clearfix flex-container align-middle">
								<div class="float-left width-50">
									<a href="{{ route('article.view', [$article->slug]) }}" title="{{ $article->title }}" class="articles__list__item__more">
										<strong>читать</strong>
									</a>
								</div>
								<div class="float-right text-right articles__list__item__date width-50">
									{{ $article->created_at }}
								</div>
							</div>
						</div>
					</div>
				</div>
			@endforeach
		</div>
		<div class="grid-x grid-margin-x">
			<div class="cell small-12 margin-bottom-1 text-center">
				{{ $articles->links() }}
			</div>
		</div>
	</div>
@endsection