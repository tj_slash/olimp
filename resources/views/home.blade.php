@extends('layouts.app')

@section('content')
	<slider-main :slides="{{ $slides->toJson() }}"></slider-main>
	<div class="grid-container margin-bottom-2">
    	<div class="grid-x grid-margin-x">
    		<div class="cell small-12 medium-5">
    			<strong>Адрес:</strong> {{ config('settings.address') }}
    		</div>
    		<div class="cell medium-2 hide-for-small-only text-center">
    			листайте вниз
    			<div class="text-center">
    				<img src="{{ asset('/uploads/images/down.svg') }}">
    			</div>
    		</div>
    		<div class="cell small-12 medium-5 medium-text-right">
    			<strong>E-mail:</strong> <a href="mailto:{{ config('settings.email') }}" target="_blank">{{ config('settings.email') }}</a>
    		</div>
    	</div>
	</div>
	<div class="header_wrapper">
		<div class="grid-container header_wrapper_m">
			<h1 class="header_title">
				Почему нас выбирают
			</h1>
		</div>
	</div>
    <div class="grid-container">
		<div class="grid-x grid-margin-x about margin-top-3 margin-bottom-2 align-stretch">
			<div class="cell small-12 medium-4 margin-bottom-1 padding-top-2">
				<div class="about__block height-100 padding-2 padding-top-3">
					<img src="{{ asset('/uploads/images/block1.png') }}" class="about__block__icon">
					<h3 class="about__block__title text-uppercase margin-top-1">
						{{ $block1->title }}
					</h3>
					<hr class="about__block__hr margin-left-0">
					<div class="about__block__text">
						{!! $block1->text !!}
					</div>
				</div>
			</div>
			<div class="cell small-12 medium-4 margin-bottom-1 padding-top-2">
				<div class="about__block height-100 padding-2 padding-top-3">
					<img src="{{ asset('/uploads/images/block1.png') }}" class="about__block__icon">
					<h3 class="about__block__title text-uppercase margin-top-1">
						{{ $block2->title }}
					</h3>
					<hr class="about__block__hr margin-left-0">
					<div class="about__block__text">
						{!! $block2->text !!}
					</div>
				</div>
			</div>
			<div class="cell small-12 medium-4 margin-bottom-1 padding-top-2">
				<div class="about__block height-100 padding-2 padding-top-3">
					<img src="{{ asset('/uploads/images/block1.png') }}" class="about__block__icon">
					<h3 class="about__block__title text-uppercase margin-top-1">
						{{ $block3->title }}
					</h3>
					<hr class="about__block__hr margin-left-0">
					<div class="about__block__text">
						{!! $block3->text !!}
					</div>
				</div>
			</div>
		</div>
	</div>
	<slider-products :slides="{{ $categories->toJson() }}"></slider-products>
	<div class="header_wrapper margin-bottom-2">
		<div class="grid-container header_wrapper_m">
			<h1 class="header_title">
				Статьи
			</h1>
		</div>
	</div>
	<div class="grid-container articles">
		<div class="grid-x align-stretch articles__list">
			@foreach($articles as $article)
				<div class="cell small-12 medium-3 margin-bottom-1 align-stretch">
					<div class="articles__list__item height-100">
						<a href="{{ route('article.view', [$article->slug]) }}" title="{{ $article->title }}">
							<img src="{{ route('image', [240, 120, $article->image]) }}" title="{{ $article->title }}" alt="{{ $article->title }}" class="width-100">
						</a>
						<div class="padding-1">
							<a href="{{ route('article.view', [$article->slug]) }}" title="{{ $article->title }}" class="display-block margin-bottom-1 articles__list__item__title text-uppercase">
								{{ $article->title }}
							</a>
							<p class="articles__list__item__anonce margin-bottom-1">
								{{ $article->anonce }}
							</p>
							<div class="clearfix flex-container align-middle">
								<div class="float-left width-50">
									<a href="{{ route('article.view', [$article->slug]) }}" title="{{ $article->title }}" class="articles__list__item__more">
										<strong>читать</strong>
									</a>
								</div>
								<div class="float-right text-right articles__list__item__date width-50">
									{{ $article->created_at }}
								</div>
							</div>
						</div>
					</div>
				</div>
			@endforeach
		</div>
		<div class="grid-x grid-margin-x">
			<div class="cell small-12 margin-bottom-1 text-center">
				<a href="{{ route('articles') }}" title="Статьи" class="button">
					все статьи
				</a>
			</div>
		</div>
	</div>
	<div class="contacts-map text-center">
		<div class="contacts-map__header text-center">
			<span class="contacts-map__header__title text-uppercase">
				Где мы находимся
			</span>
		</div>
		<div class="contacts-map__content width-100">
			<div class="grid-container">
				<div class="grid-x grid-margin-x  align-stretch">
					<div class="cell small-12 medium-4">
						<div class="contacts-map__content__block padding-1 text-left height-100">
							<div class="contacts-map__content__block__title text-uppercase margin-bottom-1">
								Связаться с нами
							</div>
							<p class="margin-0">
								E-mail: <a href="mailto:{{ config('settings.email') }}" target="_blank">{{ config('settings.email') }}</a>
							</p>
							<p class="margin-0">
								Телефон: <a href="tel:{{ config('settings.phone') }}" target="_blank">{{ config('settings.phone') }}</a>
							</p>
						</div>
					</div>
					<div class="cell small-12 medium-4">&nbsp;</div>
					<div class="cell small-12 medium-4">
						<div class="contacts-map__content__block padding-1 text-left height-100">
							<div class="contacts-map__content__block__title text-uppercase margin-bottom-1">
								Адрес офиса и склада
							</div>
							<p class="margin-0">
								{{ config('settings.address') }}
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A41f298f7ee2789c348612916cc2dcca639f34d99f8e234fd38bfd0bbed85160d&amp;source=constructor" width="100%" height="539" frameborder="0"></iframe>
	</div>
@endsection