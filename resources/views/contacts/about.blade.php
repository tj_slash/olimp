@extends('layouts.app')

@section('title', 'О компании')
@section('description', 'О компании')
@section('keywords', 'О компании')

@section('content')
	<div class="contacts">
		<div class="header_wrapper">
			<div class="grid-container header_wrapper_m">
				<h1 class="header_title">
					О компании
				</h1>
			</div>
		</div>
		<div class="grid-container">
			{{ Breadcrumbs::render('contacts') }}
			<div class="margin-bottom-2">
				{!! $about->text !!}
			</div>
			<div class="grid-x grid-margin-x about margin-top-3 margin-bottom-2 align-stretch">
				<div class="cell small-12 medium-4 margin-bottom-1 padding-top-2">
					<div class="about__block height-100 padding-2 padding-top-3">
						<img src="{{ asset('/uploads/images/block1.png') }}" class="about__block__icon">
						<h3 class="about__block__title text-uppercase margin-top-1">
							{{ $block1->title }}
						</h3>
						<hr class="about__block__hr margin-left-0">
						<div class="about__block__text">
							{!! $block1->text !!}
						</div>
					</div>
				</div>
				<div class="cell small-12 medium-4 margin-bottom-1 padding-top-2">
					<div class="about__block height-100 padding-2 padding-top-3">
						<img src="{{ asset('/uploads/images/block1.png') }}" class="about__block__icon">
						<h3 class="about__block__title text-uppercase margin-top-1">
							{{ $block2->title }}
						</h3>
						<hr class="about__block__hr margin-left-0">
						<div class="about__block__text">
							{!! $block2->text !!}
						</div>
					</div>
				</div>
				<div class="cell small-12 medium-4 margin-bottom-1 padding-top-2">
					<div class="about__block height-100 padding-2 padding-top-3">
						<img src="{{ asset('/uploads/images/block1.png') }}" class="about__block__icon">
						<h3 class="about__block__title text-uppercase margin-top-1">
							{{ $block3->title }}
						</h3>
						<hr class="about__block__hr margin-left-0">
						<div class="about__block__text">
							{!! $block3->text !!}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="contacts__employees padding-top-3">
			<div class="header_wrapper margin-bottom-2">
				<div class="grid-container header_wrapper_m white">
					<h2 class="header_title white">
						Наши сотрудники
					</h2>
				</div>
			</div>
			<div class="grid-container">
				<div class="grid-x grid-margin-x employees">
					@foreach($employees as $employee)
						<div class="cell small-6 medium-2 employees__employee">
							<img src="{{ route('image', [160, 160, $employee->image]) }}" title="{{ $employee->name }}" alt="{{ $employee->name }}" class="width-100 margin-bottom-1">
							<h3 class="margin-bottom-1 employees__employee__name">
								{{ $employee->name }}
							</h3>
							<div class="margin-bottom-1 employees__employee__position">
								{{ $employee->position }}
							</div>
							<div class="margin-bottom-1 employees__employee__email">
								E-mail:<br/>
								<a href="mailto:{{ $employee->email }}" class="employees__employee__link">
									{{ $employee->email }}
								</a>
							</div>
							<div class="margin-bottom-1 employees__employee__phone">
								Телефон:<br/>
								<a href="tel:{{ $employee->phone }}" class="employees__employee__link">
									{{ $employee->phone }}
								</a>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
@endsection