@extends('layouts.app')

@section('title', 'Статьи')
@section('description', 'Статьи')
@section('keywords', 'Статьи')

@section('content')
	<div class="contacts">
		<div class="header_wrapper">
			<div class="grid-container header_wrapper_m">
				<h1 class="header_title">
					Контакты
				</h1>
			</div>
		</div>
		<div class="grid-container">
			{{ Breadcrumbs::render('contacts') }}
		</div>
		<div class="contacts-map text-center">
			<div class="contacts-map__content width-100">
				<div class="grid-container">
					<div class="grid-x grid-margin-x  align-stretch">
						<div class="cell small-12 medium-4">
							<div class="contacts-map__content__block padding-1 text-left height-100">
								<div class="contacts-map__content__block__title text-uppercase margin-bottom-1">
									Связаться с нами
								</div>
								<p class="margin-0">
									E-mail: <a href="mailto:{{ config('settings.email') }}" target="_blank">{{ config('settings.email') }}</a>
								</p>
								<p class="margin-0">
									Телефон: <a href="tel:{{ config('settings.phone') }}" target="_blank">{{ config('settings.phone') }}</a>
								</p>
							</div>
						</div>
						<div class="cell small-12 medium-4">&nbsp;</div>
						<div class="cell small-12 medium-4">
							<div class="contacts-map__content__block padding-1 text-left height-100">
								<div class="contacts-map__content__block__title text-uppercase margin-bottom-1">
									Адрес офиса и склада
								</div>
								<p class="margin-0">
									{{ config('settings.address') }}
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A41f298f7ee2789c348612916cc2dcca639f34d99f8e234fd38bfd0bbed85160d&amp;source=constructor" width="100%" height="539" frameborder="0"></iframe>
		</div>
		<div class="contacts__employees padding-top-3">
			<div class="header_wrapper margin-bottom-2">
				<div class="grid-container header_wrapper_m white">
					<h2 class="header_title white">
						Наши сотрудники
					</h2>
				</div>
			</div>
			<div class="grid-container">
				<div class="grid-x grid-margin-x employees">
					@foreach($employees as $employee)
						<div class="cell small-6 medium-2 employees__employee">
							<img src="{{ route('image', [160, 160, $employee->image]) }}" title="{{ $employee->name }}" alt="{{ $employee->name }}" class="width-100 margin-bottom-1">
							<h3 class="margin-bottom-1 employees__employee__name">
								{{ $employee->name }}
							</h3>
							<div class="margin-bottom-1 employees__employee__position">
								{{ $employee->position }}
							</div>
							<div class="margin-bottom-1 employees__employee__email">
								E-mail:<br/>
								<a href="mailto:{{ $employee->email }}" class="employees__employee__link">
									{{ $employee->email }}
								</a>
							</div>
							<div class="margin-bottom-1 employees__employee__phone">
								Телефон:<br/>
								<a href="tel:{{ $employee->phone }}" class="employees__employee__link">
									{{ $employee->phone }}
								</a>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
		<div class="padding-vertical-3">
			<div class="header_wrapper margin-bottom-1">
				<div class="grid-container header_wrapper_m header_wrapper_m_left clearfix">
					<h1 class="header_title float-right">
						Отправить сообщение
					</h1>
				</div>
			</div>
			<div class="grid-container feedback">
				<form method="POST">
					<div class="grid-x grid-margin-x">
						<div class="cell small-12 medium-4">
							<input type="text" placeholder="Ваше имя" required class="feedback__input"/>
							<input type="tel" placeholder="Телефон" required class="feedback__input"/>
							<input type="email" placeholder="E-mail" required class="feedback__input"/>
							<input type="text" placeholder="Регион" class="feedback__input"/>
						</div>
						<div class="cell small-12 medium-8">
							<textarea placeholder="Текст сообщения" class="feedback__textarea" rows="5"></textarea>
							<div class="clearfix">
								<button type="submit" class="button float-right">Отправить</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection