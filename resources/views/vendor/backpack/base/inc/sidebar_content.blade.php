<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li>
	<a href="{{ backpack_url('dashboard') }}">
		<i class="fa fa-dashboard"></i>
		<span>{{ trans('backpack::base.dashboard') }}</span>
	</a>
</li>
<li>
	<a href="{{ backpack_url("elfinder") }}">
		<i class="fa fa-files-o"></i>
		<span>Файловый менеджер</span>
	</a>
</li>
<li>
	<a href="{{ backpack_url("user") }}">
		<i class="fa fa-users"></i>
		<span>Пользователи</span>
	</a>
</li>
<li>
	<a href="{{ backpack_url("page") }}">
		<i class="fa fa-file"></i>
		<span>Статичные страницы</span>
	</a>
</li>
<li>
	<a href="{{ backpack_url("article") }}">
		<i class="fa fa-copy"></i>
		<span>Статьи</span>
	</a>
</li>
<li>
	<a href="{{ backpack_url("slide") }}">
		<i class="fa fa-image"></i>
		<span>Слайды</span>
	</a>
</li>
<li class="treeview">
    <a href="#">
    	<i class="fa fa-shopping-basket"></i>
    	<span>Магазин</span>
        <span class="pull-right-container">
        	<i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
    	<li>
			<a href="{{ backpack_url("category") }}">
				<i class="fa fa-list-ul"></i>
				<span>Категории</span>
			</a>
		</li>
		<li>
			<a href="{{ backpack_url("product") }}">
				<i class="fa fa-product-hunt"></i>
				<span>Товары</span>
			</a>
		</li>
		<li>
			<a href="{{ backpack_url("parametr") }}">
				<i class="fa fa-cog"></i>
				<span>Параметры</span>
			</a>
		</li>
		<li>
			<a href="{{ backpack_url("value") }}">
				<i class="fa fa-cog"></i>
				<span>Значения</span>
			</a>
		</li>
		<li>
			<a href="{{ backpack_url("bid") }}">
				<i class="fa fa-bullhorn"></i>
				<span>Заявки на продукцию</span>
			</a>
		</li>
		<li>
			<a href="{{ backpack_url("get") }}">
				<i class="fa fa-bullhorn"></i>
				<span>Заявки на прайс-лист</span>
			</a>
		</li>
    </ul>
</li>
<li>
	<a href="{{ backpack_url("employee") }}">
		<i class="fa fa-user"></i>
		<span>Сотрудники</span>
	</a>
</li>
<li>
	<a href="{{ backpack_url("call") }}">
		<i class="fa fa-phone"></i>
		<span>Заявки на звонок</span>
	</a>
</li>
<li>
	<a href="{{ backpack_url("feedback") }}">
		<i class="fa fa-comment"></i>
		<span>Обратная связь</span>
	</a>
</li>
<li>
	<a href="{{ backpack_url("subscribe") }}">
		<i class="fa fa-envelope"></i>
		<span>Подписка на рассылку</span>
	</a>
</li>
<li>
	<a href="{{ backpack_url("setting") }}">
		<i class="fa fa-cogs"></i>
		<span>Настройки</span>
	</a>
</li>