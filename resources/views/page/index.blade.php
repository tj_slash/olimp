@extends('layouts.app')

@section('title', $page->meta_title)
@section('description', $page->meta_description)
@section('keywords', $page->meta_keywords)

@section('content')
	<div class="header_wrapper">
		<div class="grid-container header_wrapper_m">
			<h1 class="header_title">
				{{ $page->title }}
			</h1>
		</div>
	</div>
	<div class="grid-container margin-bottom-1">
		{{ Breadcrumbs::render('page', $page) }}
		<div class="padding-1 page margin-bottom-1">
			{!! $page->text !!}
		</div>
	</div>
@endsection