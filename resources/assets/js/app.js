
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Vue from 'vue';
import Vue2TouchEvents from 'vue2-touch-events';
Vue.use(Vue2TouchEvents, {
    swipeTolerance: 100,
});


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('accordion', require('./components/accordion.vue'));
Vue.component('subscribe', require('./components/subscribe.vue'));
Vue.component('header-block', require('./components/header.vue'));
Vue.component('price-list', require('./components/price-list.vue'));
Vue.component('bid', require('./components/bid.vue'));
Vue.component('slider-main', require('./components/slider-main.vue'));
Vue.component('slider-products', require('./components/slider-products.vue'));
Vue.component('gallery', require('./components/gallery.vue'));
Vue.component('call', require('./components/call.vue'));

const app = new Vue({
    el: '#app'
});
