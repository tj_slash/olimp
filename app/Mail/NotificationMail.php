<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Notification
     */
    public $notify = null;

    /**
     * Subject
     */
    public $subject = null;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($notify, $subject)
    {
        $this->notify = $notify;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@olimpmetall.ru')->subject($this->subject)->view('emails.notification');
    }
}
