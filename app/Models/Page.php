<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
use Backpack\CRUD\CrudTrait;
use App\Traits\MetaTrait;

class Page extends Model
{
    use SoftDeletes, Sluggable, CrudTrait, MetaTrait;

    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'text',
        'meta_title',
        'meta_keywords',
        'meta_description',
    ];

    /**
     * Sluggable
     *
     * @return array
     */
    public function sluggable()
	{
		return ['slug' => ['source' => 'title']];
	}
}