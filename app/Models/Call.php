<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Backpack\CRUD\CrudTrait;

class Call extends Model
{
    use SoftDeletes, CrudTrait;

    protected $table = 'calls';

    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'phone'
    ];
}
