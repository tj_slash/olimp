<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Backpack\CRUD\CrudTrait;

class Value extends Model
{
    use SoftDeletes, CrudTrait;

    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'parametr_id',
        'value'
    ];

    /**
     * Get product
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(\App\Models\Product::class);
    }

    /**
     * Get parametr
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parametr()
    {
        return $this->belongsTo(\App\Models\Parametr::class);
    }
}
