<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
use Backpack\CRUD\CrudTrait;
use App\Traits\MetaTrait;

class Category extends Model
{
    use SoftDeletes, Sluggable, CrudTrait, MetaTrait;

    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'image',
        'background',
        'text',
        'price_list',
        'meta_title',
        'meta_keywords',
        'meta_description',
    ];

    /**
     * Sluggable
     *
     * @return array
     */
    public function sluggable()
	{
		return ['slug' => ['source' => 'title']];
	}

    /**
     * Get parametrs
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function parametrs()
    {
        return $this->hasMany(Parametr::class);
    }

    public function getPriceNameAttribute()
    {
        return $this->title . '.' . \File::extension($this->price_list);
    }
}
