<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Backpack\CRUD\CrudTrait;

class Review extends Model
{
    use SoftDeletes, CrudTrait;

    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'name',
        'phone',
        'email',
        'text',
    ];

    /**
     * Get product
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(\App\Models\Product::class);
    }
}