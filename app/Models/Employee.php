<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Backpack\CRUD\CrudTrait;

class Employee extends Model
{
    use SoftDeletes, CrudTrait;

    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'position',
        'email',
        'phone',
        'image'
    ];
}
