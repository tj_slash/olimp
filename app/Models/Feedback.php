<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Backpack\CRUD\CrudTrait;

class Feedback extends Model
{
    use SoftDeletes, CrudTrait;

    /**
     * Table name
     * @var string
     */
    protected $table = 'feedbacks';

    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'phone',
        'email',
        'region',
        'text'
    ];
}
