<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
use Backpack\CRUD\CrudTrait;

class Setting extends Model
{
    use SoftDeletes, Sluggable, CrudTrait;

    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'value'
    ];

    /**
     * Sluggable
     *
     * @return array
     */
    public function sluggable()
	{
		return ['slug' => ['source' => 'title']];
	}
}
