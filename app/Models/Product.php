<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
use Backpack\CRUD\CrudTrait;
use Kyslik\ColumnSortable\Sortable;
use App\Traits\MetaTrait;

class Product extends Model
{
    use SoftDeletes, Sluggable, CrudTrait, Sortable, MetaTrait;

    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = [
        'category_id',
        'title',
        'text',
        'characters',
        'images',
        'videos',
        'price',
        'is_recommended',
        'meta_title',
        'meta_keywords',
        'meta_description',
    ];

    public $sortable = [
        'title',
        'price'
    ];

    /**
     * Casting
     *
     * @var array
     */
    protected $casts = [
        'images' => 'json',
        'videos' => 'json',
    ];

    /**
     * Sluggable
     *
     * @return array
     */
    public function sluggable()
	{
		return ['slug' => ['source' => 'title']];
	}

    /**
     * Get category
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class);
    }

    public function getImageAttribute()
    {
        return current($this->images);
    }

    // public function getImagesAttribute()
    // {
    //     if ($this->images) {
    //         return collect($this->images)->map(function($image) {
    //             return '/' . $image;
    //         });
    //     }
    //     return [];
    // }

    public function scopeSortCreated($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    public function scopeSearch($query)
    {
        $q = request()->q;
        return $query->where('title', 'LIKE', "%{$q}%")
            ->orWhere('text', 'LIKE', "%{$q}%")
            ->orWhere('meta_title', 'LIKE', "%{$q}%")
            ->orWhere('meta_keywords', 'LIKE', "%{$q}%")
            ->orWhere('meta_description', 'LIKE', "%{$q}%")
            ->orWhere('characters', 'LIKE', "%{$q}%")
            ->orderBy(\DB::raw('FIELD (title, "' . $q . '")'), 'desc');
    }

    /**
     * Get values
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function values()
    {
        return $this->hasMany(Value::class);
    }
}
