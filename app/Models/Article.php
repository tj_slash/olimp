<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
use Backpack\CRUD\CrudTrait;
use App\Traits\MetaTrait;

class Article extends Model
{
    use SoftDeletes, Sluggable, CrudTrait, MetaTrait;

    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'anonce',
        'image',
        'text',
        'is_new',
        'meta_title',
        'meta_keywords',
        'meta_description',
    ];

    /**
     * Sluggable
     *
     * @return array
     */
    public function sluggable()
    {
        return ['slug' => ['source' => 'title']];
    }

    public function scopeIsNew($query)
    {
        return $query->where('is_new', true);
    }

    public function scopeSortCreated($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    public function scopeExclude($query, Article $article)
    {
        if ($article) {
            $query->where('id', '<>', $article->id);
        }
        return $query;
    }
}