<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;

class ArticleController extends Controller
{
    /**
     * View articles
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $articles = Article::sortCreated();
        if (!$request->page || $request->page == 1) {
            $article = Article::isNew()->sortCreated()->first();
            if ($article) {
                $articles = $articles->exclude($article);
            }
        }
        $articles = $articles->paginate(8);
        return view('articles/index', compact('articles', 'article'));
    }

    /**
     * View article
     *
     * @return \Illuminate\Http\Response
     */
    public function view($slug)
    {
        $article = Article::where('slug', $slug)->first();
        return view('articles/view', compact('article'));
    }
}
