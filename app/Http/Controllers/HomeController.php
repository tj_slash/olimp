<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Employee;
use App\Models\Page;
use App\Models\Article;
use App\Models\Subscribe;
use App\Models\Get;
use App\Models\Bid;
use App\Models\Call;
use App\Models\Slide;

use App\Mail\NotificationMail;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Show the main page
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $block1 = Page::where('slug', 'shirokiy-assortiment-produkcii')->first();
        $block2 = Page::where('slug', 'predostavlyaem-polnyy-kompleks-uslug')->first();
        $block3 = Page::where('slug', 'dogovornaya-cenovaya-politika')->first();
        $articles = Article::sortCreated()->limit(4)->get();
        $slides = Slide::get();
        $categories = Category::get();
        return view('home', compact(
            'articles',
            'block1',
            'block2',
            'block3',
            'slides',
            'categories'
        ));
    }

    /**
     * Show the footer
     *
     * @return \Illuminate\Http\Response
     */
    public static function footer()
    {
        $categories = Category::all();
        return view('include/footer', compact('categories'));
    }

    /**
     * Show the header
     *
     * @return \Illuminate\Http\Response
     */
    public static function header()
    {
        return view('include/header');
    }

    public function contacts()
    {
        $employees = Employee::all();
        return view('contacts/index', compact('employees'));
    }

    public function about()
    {
        $employees = Employee::all();
        $about = Page::where('slug', 'o-kompanii')->first();
        $block1 = Page::where('slug', 'shirokiy-assortiment-produkcii')->first();
        $block2 = Page::where('slug', 'predostavlyaem-polnyy-kompleks-uslug')->first();
        $block3 = Page::where('slug', 'dogovornaya-cenovaya-politika')->first();
        return view('contacts/about', compact(
            'employees',
            'about',
            'block1',
            'block2',
            'block3'
        ));
    }

    public function subscribe(Request $request)
    {
        $subscribe = Subscribe::create($request->all());
        Mail::to(config('settings.noty_email'))->send(new NotificationMail(
            (object)[
                'Дата' => $subscribe->created_at,
                'E-mail' => $subscribe->email,
            ],
            'Новая подписка на рассылку!'
        ));
        return response()->json(['status' => true]);
    }

    public function priceList(Request $request)
    {
        $get = Get::create($request->all());
        Mail::to(config('settings.noty_email'))->send(new NotificationMail(
            (object)[
                'Дата' => $get->created_at,
                'E-mail' => $get->email,
                'Имя' => $get->name,
                'Телефон' => $get->phone,
                'Компания' => $get->company,
            ],
            'Новая заявка на прайс-лист!'
        ));
        return response()->json(['status' => true]);
    }

    public function bid(Request $request)
    {
        $params = $request->only([
            'product_id',
            'name',
            'phone',
            'email',
            'company',
            'text'
        ]);
        if ($request->file('file')) {
            $params['file'] = $request->file('file')->store('public');
        }
        $bid = Bid::create($params);
        Mail::to(config('settings.noty_email'))->send(new NotificationMail(
            (object)[
                'Дата' => $bid->created_at,
                'Товар' => $bid->product->title,
                'E-mail' => $bid->email,
                'Имя' => $bid->name,
                'Телефон' => $bid->phone,
                'Компания' => $bid->company,
                'Сообщение' => $bid->text,
            ],
            'Новая заявка на товар!'
        ));
        return response()->json(['status' => true]);
    }

    public function call(Request $request)
    {
        $call = Call::create($request->all());
        Mail::to(config('settings.noty_email'))->send(new NotificationMail(
            (object)[
                'Дата' => $call->created_at,
                'Имя' => $call->name,
                'Телефон' => $call->phone,
            ],
            'Новая заявка на звонок!'
        ));
        return response()->json(['status' => true]);
    }
}