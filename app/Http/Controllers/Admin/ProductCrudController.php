<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ProductRequest as StoreRequest;
use App\Http\Requests\ProductRequest as UpdateRequest;

class ProductCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel('App\Models\Product');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/product');
        $this->crud->setEntityNameStrings('товар', 'товары');

        $this->crud->addColumns([
            [
                'name' => 'title',
                'label' => 'Заголовок'
            ],
            [
                'name' => 'category_id',
                'label' => 'Категория',
                'type' => 'select',
                'entity' => 'category',
                'attribute' => 'title',
                'model' => 'App\Models\Category'
            ],
            [
                'name' => 'image',
                'label' => 'Изображение',
                'type' => 'image'
            ],
        ]);

        $this->crud->addFields([
            [
                'name' => 'title',
                'label' => 'Заголовок'
            ],
            [
                'name' => 'slug',
                'label' => 'Алиас',
                'attributes' => ['readonly' => 'readonly']
            ],
            [
                'name' => 'category_id',
                'label' => 'Категория',
                'type' => 'select',
                'entity' => 'category',
                'attribute' => 'title',
                'model' => 'App\Models\Category'
            ],
            [
                'name' => 'images',
                'label' => 'Изображения',
                'type' => 'browse_multiple',
                'multiple' => true,
            ],
            [
                'name' => 'videos',
                'label' => 'Link to video file on Youtube or Vimeo',
                'type' => 'video',
            ],
            [
                'name' => 'text',
                'label' => 'Описание',
                'type' => 'wysiwyg'
            ],
            [
                'name' => 'characters',
                'label' => 'Характеристики',
                'type' => 'wysiwyg'
            ],
            [
                'name' => 'is_recommended',
                'label' => 'Рекомендуем',
                'type' => 'checkbox'
            ],
            [
                'name' => 'meta_title',
                'label' => 'meta title',
                'type' => 'text'
            ],
            [
                'name' => 'meta_keywords',
                'label' => 'meta keywords',
                'type' => 'text'
            ],
            [
                'name' => 'meta_description',
                'label' => 'meta description',
                'type' => 'text'
            ]
        ]);

        $this->crud->orderBy('id', 'desc');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
