<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CallRequest as StoreRequest;
use App\Http\Requests\CallRequest as UpdateRequest;

class CallCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel('App\Models\Call');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/call');
        $this->crud->setEntityNameStrings('звонок', 'звонки');

        $baseFields = [
            [
                'name' => 'name',
                'label' => 'Имя',
                'attributes' => ['readonly' => 'readonly']
            ],
            [
                'name' => 'phone',
                'label' => 'Номер телефона',
                'attributes' => ['readonly' => 'readonly']
            ],
            [
                'name' => 'created_at',
                'label' => 'Дата/время',
                'attributes' => ['readonly' => 'readonly']
            ]
        ];

        $this->crud->addColumns($baseFields);
        $this->crud->addFields($baseFields);

        $this->crud->enableExportButtons();

        $this->crud->orderBy('id', 'desc');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
