<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\SettingRequest as StoreRequest;
use App\Http\Requests\SettingRequest as UpdateRequest;

class SettingCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel('App\Models\Setting');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/setting');
        $this->crud->setEntityNameStrings('настройки', 'настройки');

        $baseFields = [
            [
                'name' => 'title',
                'label' => 'Заголовок'
            ],
            [
                'name' => 'slug',
                'label' => 'Алиас',
            ],
            [
                'name' => 'value',
                'label' => 'Значение',
            ],
        ];

        $this->crud->addColumns($baseFields);
        $this->crud->addFields($baseFields);

        $this->crud->orderBy('id', 'desc');
        $this->crud->allowAccess(['list', 'create', 'update']);
        $this->crud->denyAccess(['reorder', 'delete']);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
