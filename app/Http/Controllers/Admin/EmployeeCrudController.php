<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\EmployeeRequest as StoreRequest;
use App\Http\Requests\EmployeeRequest as UpdateRequest;

class EmployeeCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel('App\Models\Employee');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/employee');
        $this->crud->setEntityNameStrings('сотрудник', 'сотрудники');


        $this->crud->addColumns([
            [
                'name' => 'name',
                'label' => 'Имя'
            ],
            [
                'name' => 'position',
                'label' => 'Должность'
            ],
            [
                'name' => 'image',
                'label' => 'Изображение',
                'type' => 'image'
            ],
        ]);
        $this->crud->addFields([
            [
                'name' => 'name',
                'label' => 'Имя'
            ],
            [
                'name' => 'position',
                'label' => 'Должность'
            ],
            [
                'name' => 'image',
                'label' => 'Изображение',
                'type' => 'browse'
            ],
            [
                'name' => 'email',
                'label' => 'E-mail',
                'type' => 'email'
            ],
            [
                'name' => 'phone',
                'label' => 'Номер телефона',
            ],
        ]);

        $this->crud->orderBy('id', 'desc');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
