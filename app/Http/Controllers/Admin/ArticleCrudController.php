<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ArticleRequest as StoreRequest;
use App\Http\Requests\ArticleRequest as UpdateRequest;

class ArticleCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel('App\Models\Article');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/article');
        $this->crud->setEntityNameStrings('статья', 'статьи');

        $baseFields = [
            [
                'name' => 'title',
                'label' => 'Заголовок'
            ],
            [
                'name' => 'slug',
                'label' => 'Алиас',
                'attributes' => ['readonly' => 'readonly']
            ],
            [
                'name' => 'image',
                'label' => 'Изображение',
                'type' => 'image'
            ],
        ];

        $this->crud->addColumns($baseFields);
        $this->crud->addFields($baseFields);

        $this->crud->addFields([
            [
                'name' => 'anonce',
                'label' => 'Анонс',
                'type' => 'textarea'
            ],
            [
                'name' => 'image',
                'label' => 'Изображение',
                'type' => 'browse'
            ],
            [
                'name' => 'text',
                'label' => 'Содержимое',
                'type' => 'wysiwyg'
            ],
            [
                'name' => 'is_new',
                'label' => 'Свежая статья',
                'type' => 'checkbox'
            ],
            [
                'name' => 'meta_title',
                'label' => 'meta title',
                'type' => 'text'
            ],
            [
                'name' => 'meta_keywords',
                'label' => 'meta keywords',
                'type' => 'text'
            ],
            [
                'name' => 'meta_description',
                'label' => 'meta description',
                'type' => 'text'
            ]
        ]);

        $this->crud->orderBy('id', 'desc');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
