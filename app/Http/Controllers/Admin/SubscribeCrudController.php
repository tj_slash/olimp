<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\SubscribeRequest as StoreRequest;
use App\Http\Requests\SubscribeRequest as UpdateRequest;

class SubscribeCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel('App\Models\Subscribe');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/subscribe');
        $this->crud->setEntityNameStrings('email', 'emails');

        $baseFields = [
            [
                'name' => 'email',
                'label' => 'E-mail'
            ]
        ];

        $this->crud->addColumns($baseFields);
        $this->crud->addFields($baseFields);

        $this->crud->allowAccess(['list', 'create', 'delete']);
        $this->crud->denyAccess(['update', 'reorder']);

        $this->crud->enableExportButtons();

        $this->crud->orderBy('id', 'desc');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
