<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BidRequest as StoreRequest;
use App\Http\Requests\BidRequest as UpdateRequest;

class BidCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel('App\Models\Bid');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/bid');
        $this->crud->setEntityNameStrings('заявка', 'заявки');

        $baseFields = [
            [
                'name' => 'product_id',
                'label' => 'Товар',
                'type' => 'select',
                'entity' => 'product',
                'attribute' => 'title',
                'model' => 'App\Models\Product',
                'attributes' => ['readonly' => 'readonly']
            ],
            [
                'name' => 'name',
                'label' => 'Имя',
                'attributes' => ['readonly' => 'readonly']
            ],
            [
                'name' => 'phone',
                'label' => 'Номер телефона',
                'attributes' => ['readonly' => 'readonly']
            ],
            [
                'name' => 'email',
                'label' => 'E-mail',
                'type' => 'email',
                'attributes' => ['readonly' => 'readonly']
            ]
        ];

        $this->crud->addColumns($baseFields);
        $this->crud->addFields($baseFields);

        $this->crud->addFields([
            [
                'name' => 'company',
                'label' => 'Компания',
                'attributes' => ['readonly' => 'readonly']
            ],
            [
                'name' => 'text',
                'label' => 'Сообщение',
                'type' => 'textarea',
                'attributes' => ['readonly' => 'readonly']
            ],
            [
                'name' => 'file',
                'label' => 'Файл',
                'type' => 'browse',
                'attributes' => ['readonly' => 'readonly']
            ],
        ]);

        $this->crud->enableExportButtons();

        $this->crud->orderBy('id', 'desc');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
