<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ValueRequest as StoreRequest;
use App\Http\Requests\ValueRequest as UpdateRequest;

class ValueCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel('App\Models\Value');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/value');
        $this->crud->setEntityNameStrings('значение', 'значения');

        $baseFields = [
            [
                'name' => 'product_id',
                'label' => 'Товар',
                'type' => 'select',
                'entity' => 'product',
                'attribute' => 'title',
                'model' => 'App\Models\Product'
            ],
            [
                'name' => 'parametr_id',
                'label' => 'Параметр',
                'type' => 'select',
                'entity' => 'parametr',
                'attribute' => 'title',
                'model' => 'App\Models\Parametr'
            ],
            [
                'name' => 'value',
                'label' => 'Значение'
            ],
        ];

        $this->crud->addColumns($baseFields);
        $this->crud->addFields($baseFields);

        $this->crud->orderBy('id', 'desc');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
