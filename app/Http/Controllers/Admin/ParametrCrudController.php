<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ParametrRequest as StoreRequest;
use App\Http\Requests\ParametrRequest as UpdateRequest;

class ParametrCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel('App\Models\Parametr');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/parametr');
        $this->crud->setEntityNameStrings('параметр', 'параметры');

        $baseFields = [
            [
                'name' => 'title',
                'label' => 'Заголовок'
            ],
            [
                'name' => 'category_id',
                'label' => 'Категория',
                'type' => 'select',
                'entity' => 'category',
                'attribute' => 'title',
                'model' => 'App\Models\Category'
            ],
        ];

        $this->crud->addColumns($baseFields);
        $this->crud->addFields($baseFields);

        $this->crud->orderBy('id', 'desc');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
