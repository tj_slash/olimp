<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\SlideRequest as StoreRequest;
use App\Http\Requests\SlideRequest as UpdateRequest;

class SlideCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel('App\Models\Slide');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/slide');
        $this->crud->setEntityNameStrings('слайд', 'слайды');

        $this->crud->addColumns([
            [
                'name' => 'title',
                'label' => 'Заголовок'
            ],
            [
                'name' => 'image',
                'label' => 'Изображение',
                'type' => 'image'
            ],
        ]);
        $this->crud->addFields([
            [
                'name' => 'title',
                'label' => 'Заголовок'
            ],
            [
                'name' => 'url',
                'label' => 'Ссылка'
            ],
            [
                'name' => 'image',
                'label' => 'Изображение',
                'type' => 'browse'
            ],
        ]);

        $this->crud->orderBy('id', 'desc');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
