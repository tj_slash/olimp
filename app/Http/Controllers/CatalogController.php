<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Parametr;
use App\Models\Value;
use App\Models\Product;

class CatalogController extends Controller
{
    /**
     * View catalog
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $products = Product::sortable(['title'])->paginate(11);
        return view('catalog/index', compact('categories', 'products'));
    }

    /**
     * View search
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
        $categories = Category::all();
        $products = Product::sortable()->search()->paginate(11);
        return view('catalog/search', compact('categories', 'products'));
    }

    /**
     * View category
     *
     * @return \Illuminate\Http\Response
     */
    public function category($category_slug)
    {
        $category = Category::where('slug', $category_slug)->first();
        $products = Product::where('category_id', $category->id)
            ->sortable(['title'])
            ->paginate(12);
        return view('catalog/category', compact('category', 'products'));
    }

    /**
     * View product
     *
     * @return \Illuminate\Http\Response
     */
    public function product($category_slug, $product_slug)
    {
        $categories = Category::all();
        $product = Product::where('slug', $product_slug)->first();
        return view('catalog/product', compact('categories', 'product'));
    }

    /**
     * View recomended products
     *
     * @return \Illuminate\Http\Response
     */
    public static function recomended(Product $product)
    {
        $products = Product::orderBy('is_recommended', 'desc')
            ->limit(4)
            ->get();
        return view('catalog/recomended', compact('products'));
    }
}
