<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;

class PageController extends Controller
{
    /**
     * Show the static page
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug)
    {
        $page = Page::where('slug', $slug)->first();
        return view('page/index', compact('page'));
    }
}