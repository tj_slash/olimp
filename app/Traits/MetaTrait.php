<?php
namespace App\Traits;

trait MetaTrait
{
    /**
     * Get meta title
     *
     * @return string
     */
    public function getMetaTitleAttribute($meta_title)
    {
        return !empty($meta_title) ? $meta_title : $this->title;
    }

    /**
     * Get meta description
     *
     * @return string
     */
    public function getMetaDescriptionAttribute($meta_description)
    {
        return !empty($meta_description) ? $meta_description : substr($this->text, 0, 250);
    }

    /**
     * Get meta keywords
     *
     * @return string
     */
    public function getMetaKeywordsAttribute($meta_keywords)
    {
        return !empty($meta_keywords) ? $meta_keywords : $this->title;
    }
}